# Доработайте классы Point и Line из занятия.
# Обеспечьте передачу в атрибуты x и y класса Point только чисел,
# а в атрибуты start_point и end_point класса Line только обьектов класса Point

# Реализуйте в классе Point механизм сложения таким образом,
# чтобы при сложении двух точек получался обьект класса Line

class OnlyNumberDescriptor:

    def __set__(self, instance, value):
        if isinstance(value, (int, float)):
            setattr(self,"value", value)
        else:
            raise TypeError

    def __get__(self, instance, owner):
        return getattr(self, "value")


class OnlyPointDescriptor:

    def __set__(self, instance, value):
        if isinstance(value, Point):
            setattr(self,"value", value)
        else:
            raise TypeError

    def __get__(self, instance, owner):
        return getattr(self, "value")

class Point:
    x = OnlyNumberDescriptor
    y = OnlyNumberDescriptor

    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def __add__(self, point):
        return Line(Point(self.x, self.y), Point(point.x, point.y))

    def __str__(self):
        return f"X = {self.x}, Y = {self.y}"

class Line:
    _start_point = OnlyPointDescriptor
    _end_point = OnlyPointDescriptor

    def __init__(self, start_point: Point, end_point: Point):
        self._start_point = start_point
        self._end_point = end_point

    def __str__(self):
        return f"Start Point is {self._start_point}, End Point is {self._end_point}"

def main():
    line = Line(Point(0,0), Point(3,-1))
    print(line)
    point = Point(1,2)
    print(point)
    newline = point.__add__(Point(4,5))
    print(newline)
    print(Point.__add__(Point(0,0), Point(3,4)))

main()